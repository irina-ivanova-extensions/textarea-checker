# CHANGELOG

### v3.0 28.01.2017

* Refactor the code
* Change style and design

### v2.0 2.09.2015

* Eight [8] default options of CSS code is added

### v1.0 16.12.2014

* Options page, where user can define titles and values of the rows (max 9 row - each for number keys)
* Hot keys works with numpad numbers
* In the Options user can choose does he want to generate first output right after extension opening

### v0.1 11.12.2014

Initial version with following harcoded options:
* [1] spaces
* [2] blank rows
* [3] umlauts
* [4] cyrillic
* [5] specal symbols
* [6] HTML tags
* [7] JavaScript code
