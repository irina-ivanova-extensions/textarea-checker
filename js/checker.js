﻿var option1,
	value1,
	output1,
	option2,
	value2,
	output2,
	option3,
	value3,
	output3,
	option4,
	value4,
	output4,
	option5,
	value5,
	output5,
	option6,
	value6,
	output6,
	option7,
	value7,
	output7,
	option8,
	value8,
	output8,
	option9,
	value9,
	output9,

	output;

function setParameters() {
	if (document.getElementById("cb1").checked) {
		output1=value1;
	} else {
		output1="";
	}

	if (document.getElementById("cb2").checked) {
		output2=value2;
	} else {
		output2="";
	}

	if (document.getElementById("cb3").checked) {
		output3=value3;
	} else {
		output3="";
	}

	if (document.getElementById("cb4").checked) {
		output4=value4;
	} else {
		output4="";
	}

	if (document.getElementById("cb5").checked) {
		output5=value5;
	} else {
		output5="";
	}

	if (document.getElementById("cb6").checked) {
		output6=value6;
	} else {
		output6="";
	}

	if (document.getElementById("cb7").checked) {
		output7=value7;
	} else {
		output7="";
	}

	if (document.getElementById("cb8").checked) {
		output8=value8;
	} else {
		output8="";
	}

	if (document.getElementById("cb9").checked) {
		output9=value9;
	} else {
		output9="";
	}
}

function setOutput() {
	output=output1 +
		output2 +
		output3 +
		output4 +
		output5 +
		output6 +
		output7 +
		output8 +
		output9;
}

function clearOutput() {
	var outputField=document.getElementById("output");
	outputField.innerHTML="";
}

function clearStatus() {
	document.getElementById("status").innerText="";
}

function returnStatus() {
	document.getElementById("status").innerText="Result is copied into clipboard";
}

function copy() {
	var copyDiv=document.createElement("div");
	copyDiv.contentEditable=true;
	document.body.appendChild(copyDiv);
	copyDiv.innerHTML=output;
	copyDiv.unselectable="off";
	copyDiv.focus();
	document.execCommand("SelectAll");
	document.execCommand("Copy", false, null);
	document.body.removeChild(copyDiv);

	returnStatus();
}

function returnOutput() {
	clearOutput();
	clearStatus();

	var outputField=document.getElementById("output");
	outputField.innerHTML += output;

	if (output !== "") {
		copy();
	}
}

function generateOutput() {
	chrome.storage.sync.get(function (item) {

		if (item.savedValue1 === undefined) {
			value1="4&nbsp;&nbsp;&nbsp;&nbsp;spaces<br />";
		} else {
			value1=item.savedValue1;
		}

		if (item.savedValue2 === undefined) {
			value2="<br /><br />2 rows<br />";
		} else {
			value2=item.savedValue2;
		}

		if (item.savedValue3 === undefined) {
			value3="äöüõ<br />";
		} else {
			value3=item.savedValue3;
		}

		if (item.savedValue4 === undefined) {
			value4="русский текст<br />";
		} else {
			value4=item.savedValue4;
		}

		if (item.savedValue5 === undefined) {
			value5="&#33;&#34;&#35;&#36;&#37;&#38;&#39;&#40;&#41;&#42;&#43;&#44;&#45;&#46;&#47;&#58;&#59;&#60;&#61;&#62;&#63;&#64;&#94;&#91;&#92;&#93;&#96;&#123;&#124;&#125;&#126;&#171;&#187;<br />";
		} else {
			value5=item.savedValue5;
		}

		if (item.savedValue6 === undefined) {
			value6="&lt;b&gt;bold&lt;/b&gt;<br />";
		} else {
			value6=item.savedValue6;
		}

		if (item.savedValue7 === undefined) {
			value7="&lt;script&gt;alert(&#39;vulnerability!&#39;)&#59;&lt;/script&gt;<br />";
		} else {
			value7=item.savedValue7;
		}

		if (item.savedValue8 === undefined) {
			value8="&lt;style&gt;p, h1, h2, h3, a, body, input, label {direction: rtl; unicode-bidi: bidi-override}&lt;/style&gt;<br />";
		} else {
			value8=item.savedValue8;
		}

		if (item.savedValue9 === undefined) {
			value9="";
		} else {
			value9=item.savedValue9;
		}

		setParameters();
		setOutput();
		returnOutput();
	});
}

function changeAll() {
	var boolean=document.getElementById("all").checked;

	document.getElementById("cb1").checked=boolean;
	document.getElementById("cb2").checked=boolean;
	document.getElementById("cb3").checked=boolean;
	document.getElementById("cb4").checked=boolean;
	document.getElementById("cb5").checked=boolean;
	document.getElementById("cb6").checked=boolean;
	document.getElementById("cb7").checked=boolean;
	document.getElementById("cb8").checked=boolean;
	document.getElementById("cb9").checked=boolean;
}

function listener(inputParameter) {
	if (inputParameter.id === "all") {
		if (inputParameter.addEventListener) {
			inputParameter.addEventListener("change", function () {
				changeAll();
				generateOutput();
			}, false);
		} else if (inputParameter.attachEvent) {
			inputParameter.attachEvent("change", changeAll);
		}
	} else {
		if (inputParameter.addEventListener) {
			inputParameter.addEventListener("change", generateOutput, false);
		} else if (inputParameter.attachEvent) {
			inputParameter.attachEvent("change", generateOutput);
		}
	}
}

function changeCB(cbId) {
	var cb=document.getElementById(cbId);

	if (cb.checked === true) {
		cb.checked=false;
	} else {
		cb.checked=true;
	}
}

var key0=48,
	nKey0=96,
	key1=49,
	nKey1=97,
	key2=50,
	nKey2=98,
	key3=51,
	nKey3=99,
	key4=52,
	nKey4=100,
	key5=53,
	nKey5=101,
	key6=54,
	nKey6=102,
	key7=55,
	nKey7=103,
	key8=56,
	nKey8=104,
	key9=57,
	nKey9=105;

function listenKeys(e) {
	if (e.keyCode === key0 || e.keyCode === nKey0) {
		changeCB("all");
		changeAll();
	} else if (e.keyCode === key1 || e.keyCode === nKey1) {
		changeCB("cb1");
	} else if (e.keyCode === key2 || e.keyCode === nKey2) {
		changeCB("cb2");
	} else if (e.keyCode === key3 || e.keyCode === nKey3) {
		changeCB("cb3");
	} else if (e.keyCode === key4 || e.keyCode === nKey4) {
		changeCB("cb4");
	} else if (e.keyCode === key5 || e.keyCode === nKey5) {
		changeCB("cb5");
	} else if (e.keyCode === key6 || e.keyCode === nKey6) {
		changeCB("cb6");
	} else if (e.keyCode === key7 || e.keyCode === nKey7) {
		changeCB("cb7");
	} else if (e.keyCode === key8 || e.keyCode === nKey8) {
		changeCB("cb8");
	} else if (e.keyCode === key9 || e.keyCode === nKey9) {
		changeCB("cb9");
	}

	generateOutput();
}

function listenParameters() {
	listener(document.getElementById("all"));
	listener(document.getElementById("cb1"));
	listener(document.getElementById("cb2"));
	listener(document.getElementById("cb3"));
	listener(document.getElementById("cb4"));
	listener(document.getElementById("cb5"));
	listener(document.getElementById("cb6"));
	listener(document.getElementById("cb7"));
	listener(document.getElementById("cb8"));
	listener(document.getElementById("cb9"));
}

function listen() {
	chrome.storage.sync.get(function (item) {

	if (item.savedDefault === true) {
		changeCB("all");
		changeAll();
		generateOutput();
	}
		setLabels();
	});

	listenParameters();
}

if (window.addEventListener) {
	window.addEventListener("load", listen, false);
	window.addEventListener("keydown", listenKeys, false);
} else if (window.attachEvent) {
	window.attachEvent("onload", listen);
	window.addEventListener("keydown", listenKeys);
} else {
	document.addEventListener("load", listen, false);
	window.attachEvent("keydown", listenKeys);
}

function setLabels() {
	if (option1 === undefined) {
		document.getElementById("label1").innerText="spaces";
	} else {
		document.getElementById("label1").innerText=option1;
	}

	if (option2 === undefined) {
		document.getElementById("label2").innerText="lines";
	} else {
		document.getElementById("label2").innerText=option2;
	}

	if (option3 === undefined) {
		document.getElementById("label3").innerText="umlauts";
	} else {
		document.getElementById("label3").innerText=option3;
	}

	if (option4 === undefined) {
		document.getElementById("label4").innerText="cyrillic";
	} else {
		document.getElementById("label4").innerText=option4;
	}

	if (option5 === undefined) {
		document.getElementById("label5").innerText="symbols";
	} else {
		document.getElementById("label5").innerText=option5;
	}

	if (option6 === undefined) {
		document.getElementById("label6").innerText="HTML";
	} else {
		document.getElementById("label6").innerText=option6;
	}

	if (option7 === undefined) {
		document.getElementById("label7").innerText="JavaScript";
	} else {
		document.getElementById("label7").innerText=option7;
	}

	if (option8 === undefined) {
		document.getElementById("label8").innerText="CSS";
	} else {
		document.getElementById("label8").innerText=option8;
	}

	if (option9 === undefined) {
		document.getElementById("label9").innerText="";
	} else {
		document.getElementById("label9").innerText=option9;
	}

}

function hideEmptyCheckboxes() {
	if (document.getElementById("label1").innerText === "") {
		document.getElementById("cb1").style.display="none";
		document.getElementById("key1").innerText="";
	}

	if (document.getElementById("label2").innerText === "") {
    	document.getElementById("cb2").style.display="none";
    	document.getElementById("key2").innerText="";
    }

    if (document.getElementById("label3").innerText === "") {
    	document.getElementById("cb3").style.display="none";
    	document.getElementById("key3").innerText="";
    }

    if (document.getElementById("label4").innerText === "") {
    	document.getElementById("cb4").style.display="none";
    	document.getElementById("key4").innerText="";
    }

    if (document.getElementById("label5").innerText === "") {
    	document.getElementById("cb5").style.display="none";
    	document.getElementById("key5").innerText="";
    }

    if (document.getElementById("label6").innerText === "") {
    	document.getElementById("cb6").style.display="none";
    	document.getElementById("key6").innerText="";
    }

    if (document.getElementById("label7").innerText === "") {
    	document.getElementById("cb7").style.display="none";
    	document.getElementById("key7").innerText="";
    }

    if (document.getElementById("label8").innerText === "") {
        document.getElementById("cb8").style.display="none";
        document.getElementById("key8").innerText="";
    }

    if (document.getElementById("label9").innerText === "") {
      	document.getElementById("cb9").style.display="none";
      	document.getElementById("key9").innerText="";
    }
}

chrome.storage.sync.get(function (item) {
	option1=item.savedOption1;
	option2=item.savedOption2;
	option3=item.savedOption3;
	option4=item.savedOption4;
	option5=item.savedOption5;
	option6=item.savedOption6;
	option7=item.savedOption7;
	option8=item.savedOption8;
	option9=item.savedOption9;

	setLabels();
	hideEmptyCheckboxes();
});
