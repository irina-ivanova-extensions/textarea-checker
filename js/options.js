﻿function save_options() {
	var status=document.getElementById("status"),
	error=document.getElementById("error"),
	defaultCB=document.getElementById("default").checked,

	option1=document.getElementById("option1").value,
	value1=document.getElementById("value1").value,
	option2=document.getElementById("option2").value,
	value2=document.getElementById("value2").value,
	option3=document.getElementById("option3").value,
	value3=document.getElementById("value3").value,
	option4=document.getElementById("option4").value,
	value4=document.getElementById("value4").value,
	option5=document.getElementById("option5").value,
	value5=document.getElementById("value5").value,
	option6=document.getElementById("option6").value,
	value6=document.getElementById("value6").value,
	option7=document.getElementById("option7").value,
	value7=document.getElementById("value7").value,
	option8=document.getElementById("option8").value,
	value8=document.getElementById("value8").value,
	option9=document.getElementById("option9").value,
	value9=document.getElementById("value9").value;

	chrome.storage.sync.set({
		savedOption1: option1,
		savedValue1: value1,
		savedOption2: option2,
		savedValue2: value2,
		savedOption3: option3,
		savedValue3: value3,
		savedOption4: option4,
		savedValue4: value4,
		savedOption5: option5,
		savedValue5: value5,
		savedOption6: option6,
		savedValue6: value6,
		savedOption7: option7,
		savedValue7: value7,
		savedOption8: option8,
		savedValue8: value8,
		savedOption9: option9,
		savedValue9: value9,
		savedDefault: defaultCB
	}, function () {
		status.textContent="Options are saved";
		setTimeout(function () {
			status.textContent="";
		}, 2000);
	});
}

function restore_options() {
	chrome.storage.sync.get({
		savedOption1: "spaces",
		savedValue1: "4&nbsp;&nbsp;&nbsp;&nbsp;spaces<br />",
		savedOption2: "lines",
		savedValue2: "<br /><br />2 rows<br />",
		savedOption3: "umlauts",
		savedValue3: "äöüõ<br />",
		savedOption4: "cyrillic",
		savedValue4: "русский текст<br />",
		savedOption5: "symbols",
		savedValue5: "&#33;&#34;&#35;&#36;&#37;&#38;&#39;&#40;&#41;&#42;&#43;&#44;&#45;&#46;&#47;&#58;&#59;&#60;&#61;&#62;&#63;&#64;&#94;&#91;&#92;&#93;&#96;&#123;&#124;&#125;&#126;&#171;&#187;<br />",
		savedOption6: "HTML",
		savedValue6: "&lt;b&gt;bold&lt;/b&gt;<br />",
		savedOption7: "JavaScript",
		savedValue7: "&lt;script&gt;alert(&#39;vulnerability!&#39;)&#59;&lt;/script&gt;<br />",
		savedOption8: "CSS",
		savedValue8: "&lt;style&gt;p, h1, h2, h3, a, body, input, label {direction: rtl; unicode-bidi: bidi-override}&lt;/style&gt;",
		savedOption9: "",
		savedValue9: "",
		savedDefault: false
	}, function (items) {
		document.getElementById("option1").value=items.savedOption1;
		document.getElementById("value1").value=items.savedValue1;
		document.getElementById("option2").value=items.savedOption2;
		document.getElementById("value2").value=items.savedValue2;
		document.getElementById("option3").value=items.savedOption3;
		document.getElementById("value3").value=items.savedValue3;
		document.getElementById("option4").value=items.savedOption4;
		document.getElementById("value4").value=items.savedValue4;
		document.getElementById("option5").value=items.savedOption5;
		document.getElementById("value5").value=items.savedValue5;
		document.getElementById("option6").value=items.savedOption6;
		document.getElementById("value6").value=items.savedValue6;
		document.getElementById("option7").value=items.savedOption7;
		document.getElementById("value7").value=items.savedValue7;
		document.getElementById("option8").value=items.savedOption8;
		document.getElementById("value8").value=items.savedValue8;
		document.getElementById("option9").value=items.savedOption9;
		document.getElementById("value9").value=items.savedValue9;
		document.getElementById("default").checked=items.savedDefault;
	});
}
document.addEventListener("DOMContentLoaded", restore_options);
document.getElementById("save").addEventListener("click",
	save_options);
