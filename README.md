## NB! This is a dead project! It's not supported anymore, so there is no guarantee that it will work on latest versions of browser.

# Short Description
Extension generates data for checking textarea field.

It's useful for web testers or developers, who want to check does special symbols in textarea HTML field are being processed properly.

# Download and Install
* **[Install](https://chrome.google.com/webstore/detail/textarea-checker/gomnbfgdoefjhpahcbahpeppplelepdd "Install")** last version from Google Store

# Questions and Comments
Any questions or comments are welcome! You can write me an e-mail on [irina.ivanova@protonmail.com](irina.ivanova@protonmail.com "irina.ivanova@protonmail.com") or create an issue here.

# Description
To generate text you need to check at least one option. Result will be automatically copied into clipboard, so You don't even need to click Ctrl+C.

*Hot keys*
* **[0]** check/uncheck all checkboxes
* **[1]-[9]** groups of symbols that user can define in Options (only not null titles are visible)

# Options
* **Key** - hot key, that changes checkbox. Both regular and numpad numbers are working.
* **Title** - title that will be shown in the extension. Empty title is invisible in extension, so to use value title should be defined.
* **Value** - value that will be added to the generated output, if checkbox is selected.
* **Check all options by default** - if this checkbox is selected, default output of all options will be automatically generated at extension opening. That means, that users clipboard will be automatically overridden.

# Chrome Tip
You can configure hot keys for extension in the Google Chrome:
* open the extensions tab - chrome://extensions
* link "Configure commands" at the bottom
* choose an extension and type a shortcut
* now You can use it completely without a mouse!

# Posts About Textarea Checker
* *January 29, 2017* [Profile Page with HTML5 and CSS3](https://ivanova-irina.blogspot.nl/2017/01/profile-page-with-html5-and-css3.html "Profile Page with HTML5 and CSS3")
* *December 16, 2014* [Textarea Checker v1.0](http://ivanova-irina.blogspot.com/2014/12/textarea-checker-v10.html "Textarea Checker v1.0")
* *December 11, 2014* [Textarea Checker v0.1](http://ivanova-irina.blogspot.com/2014/12/textarea-checker-v01.html "Textarea Checker v0.1")
